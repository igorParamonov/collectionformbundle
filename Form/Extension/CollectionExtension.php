<?php

namespace Imp\CollectionFormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('removing_message', $options['removing_message']);
        $builder->setAttribute('add_label', $options['add_label']);
        $builder->setAttribute('callbacks', $options['callbacks']);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
//        $view->vars['allow_delete']     = $options['allow_delete'];
        $view->vars['removing_message'] = $options['removing_message'];
        $view->vars['callbacks']        = $options['callbacks'];
//        $view->vars['prototype_name']   = $options['prototype_name'];
        $view->vars['add_label']        = $options['add_label'];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'removing_message'  => null,
            'add_label'         => 'add',
            'callbacks'         => array(
                'post_remove'            => null,
                'pre_remove'             => null,
                'post_add'               => null,
                'pre_add'                => null,
                'pre_append_delete_link' => null,
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return 'collection';
    }
}